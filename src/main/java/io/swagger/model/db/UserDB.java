package io.swagger.model.db;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.exceptions.UserNullFieldException;
import lombok.*;

import java.util.UUID;

@Builder
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class UserDB {
    private UUID id;

    private String name;

    private String surname;

    private Integer age;

    private String personalId;

    private String citizenship;

    private String email;

    public UUID getId() {
        return id;
    }

    public void validateNullFields() {
        if (id == null) {
            throw new UserNullFieldException("No field provided: id");
        }
        if (name == null) {
            throw new UserNullFieldException("No field provided: name");
        }
        if (surname == null) {
            throw new UserNullFieldException("No field provided: surname");
        }
        if (age == null) {
            throw new UserNullFieldException("No field provided: age");
        }
        if (personalId == null) {
            throw new UserNullFieldException("No field provided: personalId");
        }
        if (citizenship == null) {
            throw new UserNullFieldException("No field provided: citizenship");
        }
        if (email == null) {
            throw new UserNullFieldException("No field provided: email");
        }

    }
}
