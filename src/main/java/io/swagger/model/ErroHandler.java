package io.swagger.model;

import com.auth0.jwt.exceptions.SignatureVerificationException;
import io.swagger.exceptions.*;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;

import java.lang.reflect.InvocationTargetException;
import java.security.SignatureException;
import java.util.Date;
import java.util.UUID;

@ControllerAdvice
public class ErroHandler {

    @ExceptionHandler(RuntimeException.class)
    public ResponseEntity<Error> handleCityNotFoundException(RuntimeException ex, WebRequest request) {
        Error error = new Error();
        error.setResponseHeader(new ResponseHeader().sendDate(new Date()).requestId(UUID.randomUUID()));
        error.code("500");
        error.message("An error occured when performing the request");
        return new ResponseEntity<Error>(error, HttpStatus.INTERNAL_SERVER_ERROR);
    }
    @ExceptionHandler(HttpMessageNotReadableException.class)
    public ResponseEntity<Error> handleCityNotFoundException2(HttpMessageNotReadableException ex, WebRequest request) {
        Error error = new Error();
        error.setResponseHeader(new ResponseHeader().sendDate(new Date()).requestId(UUID.randomUUID()));
        error.code("400");
        error.message(ex.getMessage());
        return new ResponseEntity<Error>(error, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(ClientAlreadyExistsException.class)
    public ResponseEntity<Error> handleCityNotFoundException3(ClientAlreadyExistsException ex, WebRequest request) {
        Error error = new Error();
        error.setResponseHeader(new ResponseHeader().sendDate(new Date()).requestId(UUID.randomUUID()));
        error.code("USER_ALREADY_EXISTS");
        error.message(ex.getMessage());
        return new ResponseEntity<Error>(error, HttpStatus.UNPROCESSABLE_ENTITY);
    }

    @ExceptionHandler(ClientDoesNotExistException.class)
    public ResponseEntity<Error> handleClientDoesNotExistException(ClientDoesNotExistException ex, WebRequest request) {
        Error error = new Error();
        error.setResponseHeader(new ResponseHeader().sendDate(new Date()).requestId(UUID.randomUUID()));
        error.code("USER_DOES_NOT_EXIST");
        error.message(ex.getMessage());
        return new ResponseEntity<Error>(error, HttpStatus.UNPROCESSABLE_ENTITY);
    }

    @ExceptionHandler(InvalidCitizenshipParseException.class)
    public ResponseEntity<Error> handleInvalidCitizenshipParseException(InvalidCitizenshipParseException ex, WebRequest request) {
        Error error = new Error();
        error.setResponseHeader(new ResponseHeader().sendDate(new Date()).requestId(UUID.randomUUID()));
        error.code("INVALID_CITIZENSHIP");
        error.message(ex.getMessage());
        return new ResponseEntity<Error>(error, HttpStatus.UNPROCESSABLE_ENTITY);
    }

    @ExceptionHandler(UserNullFieldException.class)
    public ResponseEntity<Error> handleNullFieldException(UserNullFieldException ex, WebRequest request) {
        Error error = new Error();
        error.setResponseHeader(new ResponseHeader().sendDate(new Date()).requestId(UUID.randomUUID()));
        error.code("NULL_FIELD");
        error.message(ex.getMessage());
        return new ResponseEntity<Error>(error, HttpStatus.UNPROCESSABLE_ENTITY);
    }

    @ExceptionHandler(InvalidCredentialsException.class)
    public ResponseEntity<Error> handleInvalidCredentialsException(InvalidCredentialsException ex, WebRequest request) {
        Error error = new Error();
        error.setResponseHeader(new ResponseHeader().sendDate(new Date()).requestId(UUID.randomUUID()));
        error.code("INVALID_CREDENTIALS");
        error.message(ex.getMessage());
        return new ResponseEntity<Error>(error, HttpStatus.UNPROCESSABLE_ENTITY);
    }

    @ExceptionHandler(SignatureVerificationException.class)
    public ResponseEntity<Error> handleSignatureVerificationException(SignatureVerificationException ex, WebRequest request) {
        Error error = new Error();
        error.setResponseHeader(new ResponseHeader().sendDate(new Date()).requestId(UUID.randomUUID()));
        error.code("INVALID_SIGNATURE");
        error.message(ex.getMessage());
        return new ResponseEntity<Error>(error, HttpStatus.UNPROCESSABLE_ENTITY);
    }

    @ExceptionHandler(SignatureException.class)
    public ResponseEntity<Error> handleSignatureException(SignatureException ex, WebRequest request) {
        Error error = new Error();
        error.setResponseHeader(new ResponseHeader().sendDate(new Date()).requestId(UUID.randomUUID()));
        error.code("INVALID_SIGNATURE");
        error.message(ex.getMessage());
        return new ResponseEntity<Error>(error, HttpStatus.UNPROCESSABLE_ENTITY);
    }

    @ExceptionHandler(InvocationTargetException.class)
    public ResponseEntity<Error> handleInvocationTargetException(InvocationTargetException ex, WebRequest request) {
        Error error = new Error();
        error.setResponseHeader(new ResponseHeader().sendDate(new Date()).requestId(UUID.randomUUID()));
        error.code("INVALID_SIGNATURE");
        error.message(ex.getMessage());
        return new ResponseEntity<Error>(error, HttpStatus.UNPROCESSABLE_ENTITY);
    }

    @ExceptionHandler(TokenExpiredException.class)
    public ResponseEntity<Error> handleTokenExpiredException(TokenExpiredException ex, WebRequest request) {
        Error error = new Error();
        error.setResponseHeader(new ResponseHeader().sendDate(new Date()).requestId(UUID.randomUUID()));
        error.code("TOKEN_EXPIRED");
        error.message(ex.getMessage());
        return new ResponseEntity<Error>(error, HttpStatus.UNPROCESSABLE_ENTITY);
    }


}
