package io.swagger.exceptions;

public class ClientDoesNotExistException extends RuntimeException {
    public ClientDoesNotExistException() {
        super();
    }

    public ClientDoesNotExistException(String message) {
        super(message);
    }

    public ClientDoesNotExistException(String message, Throwable cause) {
        super(message, cause);
    }

    public ClientDoesNotExistException(Throwable cause) {
        super(cause);
    }

    protected ClientDoesNotExistException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
