package io.swagger.exceptions;

public class InvalidCitizenshipParseException  extends RuntimeException{
    public InvalidCitizenshipParseException() {
        super();
    }

    public InvalidCitizenshipParseException(String message) {
        super(message);
    }

    public InvalidCitizenshipParseException(String message, Throwable cause) {
        super(message, cause);
    }

    public InvalidCitizenshipParseException(Throwable cause) {
        super(cause);
    }

    protected InvalidCitizenshipParseException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
