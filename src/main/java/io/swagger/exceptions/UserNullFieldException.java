package io.swagger.exceptions;

public class UserNullFieldException extends RuntimeException {
    public UserNullFieldException() {
        super();
    }

    public UserNullFieldException(String message) {
        super(message);
    }

    public UserNullFieldException(String message, Throwable cause) {
        super(message, cause);
    }

    public UserNullFieldException(Throwable cause) {
        super(cause);
    }

    protected UserNullFieldException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
