package io.swagger.api;

import com.auth0.jwk.Jwk;
import com.auth0.jwk.JwkException;
import com.auth0.jwk.JwkProvider;
import com.auth0.jwk.UrlJwkProvider;
import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTVerificationException;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.util.JSONPObject;
import io.jsonwebtoken.JwtParser;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.impl.crypto.DefaultJwtSignatureValidator;
import io.swagger.annotations.ApiParam;
import io.swagger.exceptions.*;
import io.swagger.model.*;
import io.swagger.model.db.UserDB;
import io.swagger.repo.UserRepo;
import jdk.nashorn.internal.parser.JSONParser;
import org.bouncycastle.util.encoders.Hex;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import sun.misc.BASE64Decoder;

import javax.annotation.PostConstruct;
import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import javax.xml.bind.DatatypeConverter;
import java.io.*;
import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;
import java.security.*;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.security.interfaces.ECPublicKey;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.X509EncodedKeySpec;
import java.util.*;
import java.util.stream.Collectors;

import static io.jsonwebtoken.SignatureAlgorithm.RS256;

@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2021-11-03T14:30:00.307+01:00")

@Controller
public class UsersApiController implements UsersApi {
    private UserRepo userRepo;

    private final ObjectMapper objectMapper;

    private final HttpServletRequest request;

    private static final char[] HEX_ARRAY = "0123456789abcdef".toCharArray();

    @Autowired
    public UsersApiController(ObjectMapper objectMapper, HttpServletRequest request) {
        this.objectMapper = objectMapper;
        this.request = request;
    }

    @PostConstruct
    public void init(){
        List<UserDB> listOfUsers = new ArrayList<UserDB>();
        UserDB user = UserDB.builder().id(UUID.fromString("1c0f1731-d827-4f84-a19e-31875fb5fe71")).name("John").surname("Smith").age(24).personalId("97070808569")
                .citizenship("PL").email("js@gmail.com").build();
        listOfUsers.add(user);
        user = UserDB.builder().id(UUID.fromString("ac455cac-7245-44fd-8d50-8223fbced82b")).name("Eric").surname("Johnson").age(23).personalId("98080908123")
                .citizenship("DE").email("ej@gmail.com").build();
        listOfUsers.add(user);
        user = UserDB.builder().id(UUID.fromString("d9319637-5d38-4979-be4e-be5dcea6128a")).name("Nick").surname("Brown").age(25).personalId("96010108987")
                .citizenship("UK").email("nb@gmail.com").build();
        listOfUsers.add(user);
        userRepo = new UserRepo(listOfUsers);

    }

    @Override
    public Optional<ObjectMapper> getObjectMapper() {
        return Optional.ofNullable(objectMapper);
    }

    @Override
    public Optional<HttpServletRequest> getRequest() {
        return Optional.ofNullable(request);
    }

    @Override
    public ResponseEntity<UserListResponse> getAllUsers(@RequestHeader("Authorization") String credentialsb64) {
        credentialsb64 = credentialsb64.replace("Basic ", "");
        byte[] credDecoded = Base64.getDecoder().decode(credentialsb64);
        String credentials = new String(credDecoded, StandardCharsets.UTF_8);

        String[] fields = credentials.split(":", 2);

        System.out.println(fields[0]);
        System.out.println(fields[1]);

        if (!fields[0].equals("pba_user") || !fields[1].equals("123456")) {
            throw new InvalidCredentialsException("Invalid username and/or password");
        }

        List<User> Users = userRepo.getListOfUsers().stream()
                .map(p->new User(p.getId(),p.getName(),p.getSurname(),p.getAge(),p.getPersonalId(), User.CitizenshipEnum.valueOf(p.getCitizenship()),p.getEmail()))
                .collect(Collectors.toList());
        return ResponseEntity.ok().body(new UserListResponse().usersList(Users).responseHeader(new ResponseHeader().requestId(UUID.randomUUID()).sendDate(new Date(System.currentTimeMillis()))));
    }

    @Override
    public ResponseEntity<UserResponse> getUserById(@ApiParam(value = "",required=true) @PathVariable("id") UUID id) {
        if (userRepo.userExists(id) == false) {
            throw new ClientDoesNotExistException("No such user!");
        }

        User user = userRepo.getListOfUsers().stream().filter(u->u.getId().equals(id)).findFirst()
                .map(u->new User(u.getId(),u.getName(),u.getSurname(),u.getAge(),u.getPersonalId(),User.CitizenshipEnum.valueOf(u.getCitizenship()),u.getEmail())).orElse(null);
        return ResponseEntity.ok().body(new UserResponse().user(user).responseHeader(new ResponseHeader().requestId(UUID.randomUUID()).sendDate(new Date(System.currentTimeMillis()))));
    }

    public static String bytesToHex(byte[] bytes) {
        char[] hexChars = new char[bytes.length * 2];
        for (int j = 0; j < bytes.length; j++) {
            int v = bytes[j] & 0xFF;
            hexChars[j * 2] = HEX_ARRAY[v >>> 4];
            hexChars[j * 2 + 1] = HEX_ARRAY[v & 0x0F];
        }
        return new String(hexChars);
    }

    @Override
    public ResponseEntity<UserResponse> createUser(@ApiParam(value = "User object that has to be added" ,required=true )  @RequestHeader("X-HMAC-SIGNATURE") String signature, @Valid @RequestBody String body) throws JsonProcessingException, NoSuchAlgorithmException, InvalidKeyException, UnsupportedEncodingException {

        // validate signature
        String key = "123456";
        Mac mac = Mac.getInstance("HmacSHA256");
        SecretKeySpec signingKey = new SecretKeySpec(key.getBytes(StandardCharsets.UTF_8), "HmacSHA256");
        mac.init(signingKey);
        String generatedSignature = bytesToHex(mac.doFinal(body.getBytes(StandardCharsets.UTF_8)));

        if (generatedSignature == signature) {
            System.out.println("SAME");
        } else {
            System.out.println("NOT THE SAME");
        }

        // parse user
         JSONObject json = new JSONObject(body);
        JSONObject userJson = json.getJSONObject("user");

        User user = new ObjectMapper().readValue(userJson.toString(), User.class);

        if(userRepo.getListOfUsers().stream().filter(u->u.getId().equals(user.getId()) && u.getPersonalId().equals(user.getPersonalId())).findAny().orElse(null) == null)
        {
            UserDB usr = new UserDB(user.getId(),user.getName(),user.getSurname(), user.getAge(), user.getPersonalId(), user.getCitizenship().toString(),user.getEmail());
            usr.validateNullFields();

            userRepo.addNewUser(usr);

        }else{
            throw new ClientAlreadyExistsException("");
        }

        return ResponseEntity.ok().body(new UserResponse().user(user).responseHeader(new ResponseHeader().requestId(UUID.randomUUID()).sendDate(new Date(System.currentTimeMillis()))));
    }

    @Override
    public ResponseEntity<UserResponse> updateUser(@ApiParam(value = "",required=true) @PathVariable("id") UUID id,@ApiParam(value = "" ,required=true )  @Valid @RequestBody UpdateRequest body) {

        User user = body.getUser();

        if (userRepo.userExists(id) == false) {
            throw new ClientDoesNotExistException("There is no such user with the given ID!");
        }

        UserDB usr = new UserDB(user.getId(),user.getName(),user.getSurname(), user.getAge(), user.getPersonalId(), user.getCitizenship().toString(),user.getEmail());

        usr.validateNullFields();

        userRepo.updateUserById(id, usr);
        return ResponseEntity.ok().header("tets", "testtttt").build();
    }

    @Override
    public ResponseEntity<Void> deleteUser(@ApiParam(value = "",required=true) @PathVariable("id") UUID id, @RequestHeader("token") String token) throws JwkException, NoSuchAlgorithmException, InvalidKeySpecException, IOException, CertificateException, NoSuchProviderException, InvalidKeyException, SignatureException {
        Base64.Decoder decoder = Base64.getDecoder();
        Base64.Decoder urlDecoder = Base64.getUrlDecoder();

        String cert = "MIIDQDCCAiigAwIBAgIEX8EtRzANBgkqhkiG9w0BAQsFADBiMQswCQYDVQQGEwJQTDELMAkGA1UECAwCWlMxETAPBgNVBAcMCFN6Y3plY2luMQswCQYDVQQKDAJXSTEMMAoGA1UECwwDWlVUMRgwFgYDVQQDDA9QQkEgQVVUSCBTRVJWRVIwHhcNMjAxMTI3MTY0NTU5WhcNMjExMTI3MTY0NTU5WjBiMQswCQYDVQQGEwJQTDELMAkGA1UECAwCWlMxETAPBgNVBAcMCFN6Y3plY2luMQswCQYDVQQKDAJXSTEMMAoGA1UECwwDWlVUMRgwFgYDVQQDDA9QQkEgQVVUSCBTRVJWRVIwggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQDEFcp+Uic4iKcGvZjSsQH1WQOn/5vNcwHRw+v3jAtSxXa5jzAjSPYmiuYmZTYmU1aIiCckVU0HMWG85NPp55Evvb54odYKJnPYUoRyNNM+3XkF2Pvwd7lYvPcHl7MK9kylgdszz41DXAKRC3cb9ku3FnvWGPrRXT9HFc/WW0VJxncgYXM2kjWfDXV+hBPN47GaBi7SK6ohBdgFroilsFHZUpwpdr1rgzh7aMHoWKx+cRp7vTUqGaMcw+jelTDNG2txJ6AFOa0QJBpbrrImJtexoSsvPhHSUSXKMCDy4PghkuueLbpXXYeot6tVjeC5GblTaz1TYcEMpWiEP99NMnQzAgMBAAEwDQYJKoZIhvcNAQELBQADggEBAC1Re3Fh6BmMuX+rdu3OWbX9WONw7xYTWaXDvGtg/qczTIp4DA6YlxpTCMLANnepHpk4O9b1ml2ukWzymq+YuT4XzBZU2RtHwtHqaal/KTHGYsVY9t8W6aUEArPdrUeQ3bIzj19KZbRawlA9o6tWRDBDnF8fPAxNLz0YjWHAhZC5TgPbmgWcTOQ5ddrJ5vrQWI9spRtWCuAXLz1dBgqujtBgTls5eU1nYWkH7Wy42TePWKIJDbIwQrb8wJWih/7BS2O0Skpa3T8Z3mryIfoaLZLrY9tn5sBXl3fILwce+Or6NDTV0toBb2gNTBJNNei+0jKD9yoAl8ffxN+o8x4uzYg=";

        String[] tokenSplitted = token.split("\\.");

        byte[] payloadDecoded = decoder.decode(tokenSplitted[1]);
        String payloadDecodedString = new String(payloadDecoded);
        byte[] signatureDecoded = urlDecoder.decode(tokenSplitted[2]);

        JSONObject json = new JSONObject(payloadDecodedString);
        long exp = json.getLong("exp");
        long unixTime = System.currentTimeMillis() / 1000L;

        if (unixTime > exp) {
            throw new TokenExpiredException("");
        }

        byte[] certBytes = cert.getBytes(java.nio.charset.StandardCharsets.UTF_8);

        byte[] encodedCert = cert.getBytes("UTF-8");
        byte[] decodedCert = decoder.decode(encodedCert);
        CertificateFactory certFactory = CertificateFactory.getInstance("X.509");
        InputStream in = new ByteArrayInputStream(decodedCert);
        X509Certificate certificate = (X509Certificate)certFactory.generateCertificate(in);

        PublicKey publicKey = certificate.getPublicKey();

        final Algorithm algorithm = Algorithm.RSA256((RSAPublicKey) publicKey, null);
        algorithm.verify(JWT.decode(token));

        if (userRepo.deleteUserById(id) == false) {
            throw new ClientDoesNotExistException("");
        }

        return ResponseEntity.ok().build();
    }
}
