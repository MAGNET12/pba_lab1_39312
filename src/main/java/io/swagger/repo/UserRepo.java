package io.swagger.repo;

import io.swagger.model.db.UserDB;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Repository
@NoArgsConstructor
@Getter
public class UserRepo {
    private List<UserDB> listOfUsers;


    public UserRepo(List<UserDB> listOfUsers) {

        this.listOfUsers = new ArrayList<UserDB>();
        this.listOfUsers = listOfUsers;
    }
    public void addNewUser(UserDB newUser){
        this.listOfUsers.add(newUser);
    }
    public boolean userExists(UUID id) {
        return listOfUsers.stream().filter(user -> user.getId().equals(id)).findFirst().isPresent();
    }
    public boolean deleteUserById(UUID id) {
        return listOfUsers.removeIf(user -> user.getId().equals(id));
    }
    public boolean updateUserById(UUID id, UserDB user) {
        if (userExists(id)) {
            deleteUserById(id);
            addNewUser(user);
            return true;
        }
        return false;
    }
}
